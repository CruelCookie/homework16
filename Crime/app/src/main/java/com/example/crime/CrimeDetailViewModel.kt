package com.example.crime

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CrimeDetailViewModel: ViewModel() {
    val data:MutableLiveData<Date> by lazy{
        MutableLiveData<Date>()
    }
}