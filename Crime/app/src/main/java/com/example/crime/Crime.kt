package com.example.crime

data class Crime(
    var title: String,
    var id: Int,
    var data: Date,
    var isSolved: Boolean
)
