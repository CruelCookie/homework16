package com.example.crime

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import com.example.crime.databinding.FragmentCrimeBinding

class CrimeFragment : Fragment() {
    private val dataModel: CrimeDetailViewModel by activityViewModels()
    private val crime = Crime("Murder", 0, Date(15,9,2002, Time(15,40)), true)
    private lateinit var binding: FragmentCrimeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCrimeBinding.inflate(inflater)
        binding.tvCDate.text = crime.data.toString()
        binding.tvCTitle.text = crime.title
        binding.tvCID.text = crime.id.toString()
        binding.checkBox.isChecked = crime.isSolved
        dataModel.data.value = crime.data
        binding.checkBox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                crime.isSolved = p1
            }
        })
        binding.btChange.setOnClickListener{
            val dialog = DatePickerFragment()
            dialog.show(requireActivity().supportFragmentManager, "customDialog")
        }
        binding.etTitle.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                crime.title = p0.toString()
                binding.tvCTitle.text = crime.title
            }
        })
        dataModel.data.observe(activity as LifecycleOwner){
            crime.data = dataModel.data.value!!
            binding.tvCDate.text = dataModel.data.value.toString()
        }

        return binding.root
    }

    companion object {
     @JvmStatic
        fun newInstance() = CrimeFragment()
    }
}