package com.example.crime

data class Date(
    var day: Int,
    var month: Int,
    var year: Int,
    var time: Time
){
    override fun toString(): String {
        return "$day.$month.$year " + time.toString()
    }
}
