package com.example.crime

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TextView
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import com.example.crime.databinding.FragmentDataDialogBinding

class DatePickerFragment: DialogFragment() {
    private lateinit var binding: FragmentDataDialogBinding
    private val dataModel:CrimeDetailViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDataDialogBinding.inflate(inflater)
        binding.tvCurrent.text = dataModel.data.value.toString()
        binding.btCancel.setOnClickListener {
            dismiss()
        }
        dataModel.data.observe(activity as LifecycleOwner){
            binding.tvCurrent.text = dataModel.data.value.toString()
        }
        binding.btDate.setOnClickListener {
            btSelectDate()
        }
        binding.btTime.setOnClickListener {
            btSelectTime()
        }
        return binding.root
    }

    private fun btSelectTime() {
        val timeSetListener = object : TimePickerDialog.OnTimeSetListener{
            override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
                val data = dataModel.data.value
                dataModel.data.value = Date(data?.day!!, data.month, data.year, Time(p1,p2))
            }
        }
        val timePickerDialog = TimePickerDialog(requireContext(),timeSetListener,
            dataModel.data.value?.time!!.hours,dataModel.data.value?.time!!.minutes,true)
        timePickerDialog.show()
    }

    private fun btSelectDate() {
        val dateSetListner = object : DatePickerDialog.OnDateSetListener{
            override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
                val data = dataModel.data.value
                dataModel.data.value = Date(p3, p2, p1, data?.time!!)
            }
        }
        val datePickerDialog = DatePickerDialog(requireContext(),android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
            dateSetListner,dataModel.data.value?.year!!,dataModel.data.value?.month!!,dataModel.data.value?.day!!)
        datePickerDialog.show()
    }
}